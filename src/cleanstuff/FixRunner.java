package cleanstuff;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class FixRunner {
	public static void main(String args[]) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.postgresql.Driver");
			c = DriverManager
					.getConnection("jdbc:postgresql://" + args[0] + "/" + args[1], args[2], args[3]);
			System.out.println("Opened database successfully");

			stmt = c.createStatement();
			String sql = "select count(*) from batch_instance";

			var rs = stmt.executeQuery(sql);
			rs.next();
			System.out.println(rs.getFloat(1));
			stmt.close();
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.exit(0);
		}
		System.out.println("Table created successfully");
	}
}